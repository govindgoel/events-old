import React from "react"

const Map = () => {

  return(
    <section id="venue-details">
      <div className="row m-0 w-100">
        <div className="col-lg-8 order-2 p-4">
        <div className="fs-4 p-4"><h3>Get Ready For Exciting Events and Workshops</h3></div>
        </div>
        <div className="col p-4">
          <h2>Virtual Meetup</h2>
          <h3>Amrita School of Engineering,</h3>
          <div className="fs-4">Amritapuri Campus,</div>
          <div className="fs-5">
            Platform: Hopin
          </div>
        </div>
      </div>
    </section>
  )

}

export default Map;
